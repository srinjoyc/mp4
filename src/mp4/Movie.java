package mp4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Movie {
	private final int id;
	private final String name;
	private final int releaseYear;
	private final String imdbUrl;
	
	public static HashMap<Integer,Movie> allmovies = new HashMap<Integer,Movie>();
	
	private List<Rating> likes = new ArrayList<Rating>();
	private List<Rating> dislikes = new ArrayList<Rating>();
	public int numreviews=0;
	/**
	 * Create a new Movie object with the given information.
	 * 
	 * @param id
	 *            the movie id
	 * @param name
	 *            the name of the movie
	 * @param releaseYear
	 *            the year of the movie's release
	 * @param imdbUrl
	 *            the movie's IMDb URL
	 */
	public Movie(int id, String name, int releaseYear, String imdbUrl) {
		this.id = id;
		this.name = name;
		this.releaseYear = releaseYear;
		this.imdbUrl = imdbUrl;
		allmovies.put(id,this );
	}

	/**
	 * Return the name of the movie
	 * 
	 * @return movie name
	 */
	public String getName() {
		return name;
	}
	public int getId() {
		return this.id;
	}
	public static Movie getMovie(int id) {
		Movie movie=allmovies.get(id);
		return movie;
	}
	public void addLike(Rating r1){
		numreviews++;
		this.likes.add(r1);
	}
	public void addDisLike(Rating r1){
		numreviews++;
		this.dislikes.add(r1);
	}

	public int getDLikeSize(){
		return this.dislikes.size();
	}
	
	public int getLikeSize(){
		
		return this.likes.size();
	}
	public void addreview(){
		numreviews++;
	}
	
	/**
	 * hashCode for equality testing
	 */
	@Override
	public int hashCode() {
		return id;
	}
	
	/**
	 * Method to check if two Movie objects are equal
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		if(other instanceof Movie){
			if(this.id == ((Movie)other).id && this.name.equals(((Movie)other).name)){
				return true;
			}
		}
		else{
			throw new IllegalArgumentException("The Object passed is not of the movie type");
		}
		return false;
	}
	public static void populateRatings() throws IOException {
RatingIterator iter2 = new RatingIterator("data/u.data.txt");
		//requires movies to have been created already
		while (iter2.hasNext()) {
			Rating rating = iter2.getNext();
			int movid=rating.getMovieId();
			Movie movie=getMovie(movid);
			
			if(rating.getRating()<=2) movie.addDisLike(rating);
			if(rating.getRating()>=4) movie.addLike(rating);
			if(rating.getRating()==3) movie.addreview();
			
			
		}
	}
	public static void main (String [] args){
		try {
			populateRatings();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("done");
	
	}
}
