package mp4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

// TODO: Implement this class that represents an undirected graph with movies as vertices.
// The edges are weighted.
// This graph should be immutable except for the addition of vertices and edges. 
// It should not be possible to change a vertex after it has been added to the graph.

// You should indicate what the representation invariants and the abstraction function are for the representation you choose.

public class MovieGraph {
	private static Map<Integer, ArrayList<Edge>> graph = new HashMap<Integer, ArrayList<Edge>>();
	public static Map<Integer, Movie> mlist = new HashMap<Integer, Movie>(); 
	Integer sLength = 999999;

	/**
	 * Add a new movie to the graph. If the movie already exists in the graph
	 * then this method will return false. Otherwise this method will add the
	 * movie to the graph and return true.
	 * 
	 * @param movie
	 *            the movie to add to the graph. Requires that movie != null.
	 * @return true if the movie was successfully added and false otherwise.
	 * @modifies this by adding the movie to the graph if the movie did not
	 *           exist in the graph.
	 */
	public boolean addVertex(Movie movie) {
		
		if(!graph.containsKey(movie.getId())){
			graph.put(movie.getId(), new ArrayList<Edge>());
			
			return true;
		}
		else 
			return false;
	}

	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method will return false. Otherwise this method will add the edge to
	 * the graph and return true.
	 * 
	 * @param movie1
	 *            one end of the edge being added. Requires that m1 != null.
	 * @param movie2
	 *            the other end of the edge being added. Requires that m2 !=
	 *            null. Also require that m1 is not equal to m2 because
	 *            self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(Movie movie1, Movie movie2, int edgeWeight) {
		if (movie1 == null || movie2 == null) {
            throw new NullPointerException("Either of the 2 nodes is null.");
        }
        if (edgeWeight < 0) {
            throw new IllegalArgumentException(" The distance cannot be negative. ");
        }

        Edge edge = new Edge(movie1, movie2, edgeWeight);
        graph.put(movie1.getId(),new ArrayList<Edge>());
        graph.put(movie2.getId(),new ArrayList<Edge>());
        graph.get(movie1.getId()).add(edge);
        graph.get(movie2.getId()).add(edge);
        
        System.out.println(movie1.getId());
        if(mlist.containsKey(movie1.getId())){
        graph.get(movie1.getId()).add(edge);
        }
        if(mlist.containsKey(movie1.getId())){
        graph.put(movie1.getId(),new ArrayList<Edge>());
        graph.get(movie1.getId()).add(edge);
        }
        if(!mlist.containsKey(movie1.getId()))
    		mlist.put(movie1.getId(), movie1);
        if(!mlist.containsKey(movie2.getId()))
		mlist.put(movie2.getId(), movie2);
        System.out.println(mlist);
		return false;
	}

	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method should return false. Otherwise this method should add the
	 * edge to the graph and return true.
	 * 
	 * @param movieId1
	 *            the movie id for one end of the edge being added. Requires
	 *            that m1 != null.
	 * @param movieId2
	 *            the movie id for the other end of the edge being added.
	 *            Requires that m2 != null. Also require that m1 is not equal to
	 *            m2 because self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(int movieId1, int movieId2, int edgeWeight) {
		if (!Movie.allmovies.containsKey(movieId1)|| !Movie.allmovies.containsKey(movieId1)) {
            throw new NullPointerException("Either of the 2 nodes do not exist.");
        }
        if (edgeWeight < 0) {
            throw new IllegalArgumentException(" The distance cannot be negative. ");
        }

        Edge edge = new Edge(Movie.getMovie(movieId1), Movie.getMovie(movieId2), edgeWeight);
        graph.put(movieId1,new ArrayList<Edge>());
        graph.put(movieId2,new ArrayList<Edge>());
        graph.get(movieId1).add(edge);
        graph.get(movieId2).add(edge); 
        
        
        return true;
        
        //change to id
        
//        System.out.println(movieId1);
//        if(mlist.containsKey(movieId1)){
//        graph.get(movieId1).add(edge);
//        }
//        if(mlist.containsKey(movie1.getId())){
//        graph.put(movie1.getId(),new ArrayList<Edge>());
//        graph.get(movie1.getId()).add(edge);
//        }

	}

	/**
	 * Return the length of the shortest path between two movies in the graph.
	 * Throws an exception if the movie ids do not represent valid vertices in
	 * the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the length of the shortest path between the two movies
	 *         represented by their movie ids.
	 */
	public int getShortestPathLength(int movieId1, int movieId2)
	// NOT CORRECT! by TA
			throws NoSuchMovieException, NoPathException {
		if(!graph.containsKey(movieId1) || !graph.containsKey(movieId2))
			throw new NoSuchMovieException();		
		
		
		Integer tLength = 0;
		Integer i = 0;
		Integer length2 = 0;
		ArrayList<Integer> vList = new ArrayList<Integer>();
		
		if(movieId1 == movieId2)
			return 0;
		else{
			Set<Integer> key = graph.keySet();
			ArrayList<Integer> keys = new ArrayList<Integer>(key);
			
			for(int p = 0; p < keys.size(); p++){
				for(int r=0;r<graph.get(p).size();r++){
					
					int edge1=graph.get(p).get(r).getWeight();
					
					if(edge1<sLength){
						sLength=edge1;
						i=p;
					}
					
				}
			}
		return getShortestPathLength(i, movieId2);
		}
	}

	/**
	 * Return the shortest path between two movies in the graph. Throws an
	 * exception if the movie ids do not represent valid vertices in the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the shortest path, as a list, between the two movies represented
	 *         by their movie ids. This path begins at the movie represented by
	 *         movieId1 and ends with the movie represented by movieId2.
	 */
	public List<Movie> getShortestPath(int movieId1, int movieId2)
	// NOT CORRECT! by TA
			throws NoSuchMovieException, NoPathException {
		// TODO: Implement this method
		if(!graph.containsKey(movieId1) || !graph.containsKey(movieId2))
			throw new NoSuchMovieException();
		
		List<Movie> temp = new ArrayList<Movie>();
		
		
		temp.add(mlist.get(movieId1));
		temp.add(mlist.get(movieId2));
		
		
		return temp;
	}

	/**
	 * Returns the movie id given the name of the movie. For movies that are not
	 * in English, the name contains the English transliteration original name
	 * and the English translation. A match is found if any one of the two
	 * variations is provided as input. Typically the name string has <English
	 * Translation> (<English Transliteration>) for movies that are not in
	 * English.
	 * 
	 * @param name
	 *            the movie name for the movie whose id is needed.
	 * @return the id for the movie corresponding to the name. If an exact match
	 *         is not found then return the id for the movie with the best match
	 *         in terms of translation/transliteration of the movie name.
	 * @throws NoSuchMovieException
	 *             if the name does not match any movie in the graph.
	 */
	public int getMovieId(String name) throws NoSuchMovieException {
		if(!Movie.allmovies.containsKey(name))
			throw new NoSuchMovieException();
		else{
			Movie temp = Movie.allmovies.get(name);
			return temp.getId();
		}
	}

	// Implement the next two methods for completeness of the MovieGraph ADT

	@Override
	public boolean equals(Object other) {
		if(!(other instanceof Map))
			return false;
		else if(this.graph.size() == ((MovieGraph)other).graph.size() && this.graph.get(this.graph.size()/2) == ((MovieGraph)other).graph.get(((MovieGraph)other).graph.size()/2)){
			return true;
		}
		else
			return false;

	}

	@Override
	public int hashCode() {
		// TODO: Implement a reasonable hash code method
		
		ArrayList<Integer> temp = new ArrayList(graph.keySet());
		
		return temp.get(temp.size()/4) + temp.get(temp.size()/2); 
		
	}
	
	public static void main (String [] args){
		MovieGraph t1 = new MovieGraph();
		Movie m1 = new Movie(1, "Movie 1", 1994, "www.");
		Movie m2 = new Movie(2, "Movie 3", 1999, "qwe");
		Movie m3 = new Movie(3, "Movie 2", 1998, "123");
		Movie m4 = new Movie(4, "Movie 4", 1678, "neil");
		
		t1.addEdge(m1, m2, 3);
		t1.addEdge(m1, m2, 6);
		t1.addEdge(m1, m3, 5);
		t1.addEdge(m1, m4, 6);
		t1.addEdge(m2, m4, 2);
		t1.addEdge(m3, m4, 1);
		
		System.out.println(t1.graph);
		System.out.println(t1.graph.get(m2.getId()).get(0).getWeight());
		System.out.println(t1.graph.get(m2.getId()).get(0).getRightNode());
		System.out.println(t1.graph.get(m2.getId()).get(0).getLeftNode());
		System.out.println(t1.graph.get(m2.getId()).get(0).getRightNode().getId());
		System.out.println(t1.graph.get(m2.getId()).get(0).getLeftNode().getId());
	}

}
